<?php

use App\Http\Controllers\CadastroController;
use App\Models\Cadastro;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::get('/', function () {
    return response()->json(['message' => 'API', 'status' => 'Connected']);;
});

Route::get('crud-test', 'CadastroController@index')->name('index');
Route::get('crud-test/{id}', 'CadastroController@show')->name('show');
Route::post('crud-test', 'CadastroController@store')->name('store');
Route::put('crud-test/{id}', 'CadastroController@update')->name('update');
Route::delete('crud-test/{id}', 'CadastroController@destroy')->name('destroy');
