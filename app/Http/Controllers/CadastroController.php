<?php

namespace App\Http\Controllers;

use App\Models\Cadastro;
use Illuminate\Http\Request;
use Yajra\DataTables\DataTables;

class CadastroController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        try {

            if (request()->is('api/*')) {
                $cadastros = Cadastro::paginate(10);
                return response()->json($cadastros);
            }

            if ($request->ajax()) {
                $data = Cadastro::select('*');
                return Datatables::of($data)
                    ->addIndexColumn()
                    ->addColumn('action', function ($row) {

                        $btn = '<a href="javascript:void(0)" data-id="' . $row->id . '" onclick="editCadastro(event.target)" class="btn btn-info">Editar</a>';

                        $btn = $btn . ' <a href="javascript:void(0)" data-id="' . $row->id . '" class="btn btn-danger" onclick="deleteCadastro(event.target)">Deletar</a>';

                        return $btn;
                    })
                    ->rawColumns(['action'])
                    ->make(true);
            }

            // $cadastros = Cadastro::all();
            return view('welcome');
        } catch (\Exception $e) {
            return response()->json($e->getMessage(), 400);
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        try {
            $this->validate($request, [
                'nome' => 'required|max:50',
                'situacao' => 'required|max:1',
                'nusuario_inclusao' => 'required'
            ]);
            $cadastro = Cadastro::create($request->all());
            return response()->json(['code' => 201, 'message' => 'Cadastro criado com sucesso', 'data' => $cadastro], 201);
        } catch (\Exception $e) {
            return response()->json($e->getMessage(), 400);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Cadastro  $cadastro
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        try {
            $cadastro = Cadastro::find($id);
            if (!$cadastro) {
                return response()->json([
                    'message'   => 'Registro não encontrado',
                ], 404);
            }
            return response()->json($cadastro, 200);
        } catch (\Exception $e) {
            return response()->json($e->getMessage(), 400);
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Cadastro  $cadastro
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        try {
            $this->validate($request, [
                'nome' => 'required|max:50',
                'situacao' => 'required|max:1',
                'nusuario_alteracao' => 'required'
            ]);
            Cadastro::withTrashed()->findOrFail($id)->restore();
            $cadastro = Cadastro::findOrFail($id);
            if (!$cadastro) {
                return response()->json([
                    'message'   => 'Registro não encontrado',
                ], 404);
            }
            $inputs = $request->all();
            $cadastro->fill($inputs);
            $cadastro->save();
            return response()->json(['code' => 201, 'message' => 'Cadastro atualizado com sucesso', 'data' => $cadastro], 201);
        } catch (\Exception $e) {
            return response()->json($e->getMessage(), 400);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Cadastro  $cadastro
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        try {
            $cadastro = Cadastro::findOrFail($id);
            $cadastro->delete();
            return response()->json(['success' => 'Cadastro excluído com sucesso'], 204);
        } catch (\Exception $e) {
            return response()->json($e->getMessage(), 400);
        }
    }
}
