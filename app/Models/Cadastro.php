<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Cadastro extends Model
{
    use HasFactory, SoftDeletes;

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'cadastro';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'nome',
        'situacao',
        'nusuario_inclusao',
        'nusuario_alteracao',
    ];

    protected $dates = ['criado_at', 'excluido_at', 'atualizado_at'];

    /**
     * The name of the "created at" column.
     *
     * @var string
     */
    const CREATED_AT = 'criado_at';

     /**
     * The name of the "deleted at" column.
     *
     * @var string
     */
    const DELETED_AT = 'excluido_at';

    /**
     * The name of the "updated at" column.
     *
     * @var string
     */
    const UPDATED_AT = 'atualizado_at';

}
