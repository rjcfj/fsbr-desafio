<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>Laravel</title>

    <!-- Fonts -->
    <link href="https://fonts.googleapis.com/css2?family=Nunito:wght@400;600;700&display=swap" rel="stylesheet">
    <!-- Styles -->
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/css/bootstrap.min.css"
        integrity="sha384-TX8t27EcRE3e/ihU7zmQxVncDAy5uIKz4rEkgIXeMed4M0jlfIDPvg6uqKI2xXr2" crossorigin="anonymous">
    <link rel="stylesheet" href="https://cdn.datatables.net/1.10.24/css/dataTables.bootstrap4.min.css" />
</head>

<style>
    .bd-placeholder-img {
        font-size: 1.125rem;
        text-anchor: middle;
        -webkit-user-select: none;
        -moz-user-select: none;
        -ms-user-select: none;
        user-select: none;
    }

    @media (min-width: 768px) {
        .bd-placeholder-img-lg {
            font-size: 3.5rem;
        }
    }

</style>
</head>

<body class="d-flex flex-column h-100">

    <div class="container">
        <h2 style="margin-top: 12px;">Cadastro</h2><br>
        <div class="row" style="clear: both;margin-top: 18px;">
            <div class="col-12 text-right">
                <a href="javascript:void(0)" class="btn btn-success mb-3" id="create-new-cadastro"
                    onclick="addCadastro()">Adicionar Cadastro</a>
            </div>
        </div>
        <div class="row">
            <div class="col-12">
                <table class="table table-striped table-bordered data-table">
                    <thead>
                        <tr>
                            <th>ID</th>
                            <th>Nome</th>
                            <th>Situação</th>
                            <th width="280px">Action</th>
                        </tr>
                    </thead>
                    <tbody>
                    </tbody>
                </table>
            </div>
        </div>
    </div>

    <div class="modal fade" id="cadastro-modal" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title"></h4>
                </div>
                <div class="modal-body">
                    <form name="userForm" class="form-horizontal">
                        <input type="hidden" name="cadastro_id" id="cadastro_id">
                        <div class="form-group">
                            <label for="name" class="col-sm-2">Nome</label>
                            <div class="col-sm-12">
                                <input type="text" class="form-control" id="nome" name="nome" placeholder="Nome">
                                <span id="nomeError" class="alert-message"></span>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-sm-2">Situação</label>
                            <div class="col-sm-12">
                                <input type="text" class="form-control" id="situacao" name="situacao"
                                    placeholder="Situação">
                                <span id="situacaoError" class="alert-message"></span>
                            </div>
                        </div>
                    </form>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-primary" data-dismiss="modal">Cancelar</button>
                    <button type="button" class="btn btn-primary" onclick="createCadastro()">Salvar</button>
                </div>
            </div>
        </div>
    </div>

    <!-- Scripts -->
    <script src="https://code.jquery.com/jquery-3.4.1.min.js"
        integrity="sha256-CSXorXvZcTkaix6Yvo6HppcZGetbYMGWSFlBw8HfCJo=" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.1/dist/umd/popper.min.js"
        integrity="sha384-9/reFTGAW83EW2RDu2S0VKaIzap3H66lZH81PoYlFhbGU+6BZp6G7niu735Sk7lN" crossorigin="anonymous">
    </script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/js/bootstrap.min.js"
        integrity="sha384-w1Q4orYjBQndcko6MimVbzY0tgp4pWB4lZ7lr30WKz0vr/aWKhXdBNmNb5D92v7s" crossorigin="anonymous">
    </script>
    <script src="https://cdn.datatables.net/1.10.24/js/jquery.dataTables.min.js"></script>
    <script src="https://cdn.datatables.net/1.10.24/js/dataTables.bootstrap4.min.js"></script>

    <script type="text/javascript">
        var table = $('.data-table').DataTable({
            processing: true,
            serverSide: true,
            ajax: "{{ route('web.index') }}",
            language: {
                url: '//cdn.datatables.net/plug-ins/1.10.24/i18n/Portuguese-Brasil.json'
            },
            columns: [
                {data: 'id', name: 'id'},
                {data: 'nome', name: 'nome'},
                {data: 'situacao', name: 'situacao'},
                {data: 'action', name: 'action', orderable: false, searchable: false},
            ]
        });

        function addCadastro() {
            $("#cadastro_id").val('');
            $('#nome').val('');
            $('#situacao').val('');
            $('#cadastro-modal').modal('show');
        }

        function editCadastro(event) {
            var id = $(event).data("id");
            let _url = `/api/crud-test/${id}`;
            $('#nomeError').text('');
            $('#situacaoError').text('');

            $.ajax({
                url: _url,
                type: "GET",
                success: function(response) {
                    if (response) {
                        $("#cadastro_id").val(response.id);
                        $("#nome").val(response.nome);
                        $("#situacao").val(response.situacao);
                        $('#cadastro-modal').modal('show');
                    }
                }
            });
        }

        function createCadastro() {
            var nome = $('#nome').val();
            var situacao = $('#situacao').val();
            var id = $('#cadastro_id').val();
            var datasInputs = id ? { id: id, nome: nome, situacao: situacao, nusuario_alteracao: 3 } : {nome: nome, situacao: situacao, nusuario_inclusao: 1 }
            let _url = `/api/crud-test/${id ? id : ''}`;

            $.ajax({
                url: _url,
                type: id ? "PUT" : "POST",
                data: datasInputs,
                success: function(response) {
                    console.log(response)
                    if (response.code == 201) {
                        if (id != "") {
                            $("#row_" + id + " td:nth-child(2)").html(response.data.nome);
                            $("#row_" + id + " td:nth-child(3)").html(response.data.situacao);
                        } else {
                            $('table tbody').append('<tr id="row_' + response.data.id + '"><td>' + response
                                .data.id + '</td><td>' + response.data.nome + '</td><td>' +
                                response.data.situacao +
                                '</td><td><a href="javascript:void(0)" data-id="' + response.data.id +
                                '" onclick="editCadastro(event.target)" class="btn btn-info">Editar</a></td><td><a href="javascript:void(0)" data-id="' +
                                response.data.id +
                                '" class="btn btn-danger" onclick="deleteCadastro(event.target)">Deletar</a></td></tr>'
                            );
                        }
                        $('nomee').val('');
                        $('#situacao').val('');

                        $('#cadastro-modal').modal('hide');
                        table.draw();
                    }
                },
                error: function(response) {
                    $('#nomeError').text(response.responseJSON.errors.nome);
                    $('#situacaoError').text(response.responseJSON.errors.situacao);
                }
            });
        }

        function deleteCadastro(event) {
            var id = $(event).data("id");
            let _url = `/api/crud-test/${id}`;

            $.ajax({
                url: _url,
                type: 'DELETE',
                data: {},
                success: function(response) {
                    table.draw();
                }
            });
        }

    </script>

</body>

</html>
