# Desafio - FSBR - Laravel 8.35.1
Desenvolver um serviço REST e FRONTEND

## Servidor:
Nginx 1.15.0 / PHP 7.3.6

## Instalação
~~~~
git clone https://gitlab.com/rjcfj/fsbr-desafio.git
composer update
php -r "file_exists('.env') || copy('.env.example', '.env');"
php artisan key:generate

~~~~

Você também deve adicionar suas informações de banco de dados em seu arquivo .env:
~~~~ 
BD: 
DB_CONNECTION=mysql
DB_HOST=###
DB_PORT=3306
DB_DATABASE=###
DB_USERNAME=###
DB_PASSWORD=###
        
~~~~ 

## Docker

Passo 1:
Instalar o docker compose na sua maquina.

Passo 2:
Clonar Laradock no seu Projeto laravel.
~~~~
$ git clone https://github.com/Laradock/laradock.git
~~~~

Passo 3:
Entra na pasta do Laradock renomeia o arquivo env-example por .env.
~~~~
$ cp env-example .env
~~~~

Passo 4:
Run
~~~~
$ docker-compose up -d nginx
~~~~
Isto vai levar algum tempo para criar as imagens na tua maquina.

Depois disso, tudo deve funcionar de forma normal.

Inicie um servidor de desenvolvimento local é, visite http://localhost

## FRONTEND
http://localhost/crud-test

## API (JSON)
Serviço Rest: Postman ou Insomnia 

## GET (Lista)
http://localhost/api/crud-test

## GET (Buscar)
http://localhost/api/crud-test/1

## POST
http://localhost/api/crud-test
~~~~
{"nome":"$","situacao":"#","nusuario_inclusao":"##"}
~~~~

## PUT
http://localhost/api/crud-test/1
~~~~
{"nome":"$","situacao":"#","nusuario_alteracao":"##"}
~~~~

## DELETE
http://localhost/api/crud-test/1

